public class ArbolBinario {

    private NodoArbol raiz;

    //Constructor.
    public ArbolBinario() {
        this.raiz = null;
    }

    //Metodo que dado un elemPadre, un elem y una posicion.
    //inserta elem donde determine la pos, como hijo de elemPadre.
    public boolean insertar(Object elemPadre, Object elem, char pos) {
        boolean bandera = false;
        if (this.raiz == null) {
            NodoArbol nuevo = new NodoArbol(null, null, elem);
            this.raiz = nuevo;
            bandera = true;
        } else {
            NodoArbol aux = obtenerNodo(this.raiz, elemPadre);
            if (pos == 'D' && aux.getDerecho() == null) {
                NodoArbol nodo = new NodoArbol(null, null, elem);
                aux.setDerecho(nodo);
                bandera = true;
            } else {
                if (pos == 'I' && aux.getIzquierdo() == null) {
                    NodoArbol nodo = new NodoArbol(null, null, elem);
                    aux.setIzquierdo(nodo);
                    bandera = true;
                }
            }
        }
        return bandera;

    }

    //Metodo auxiliar que devuelve un nodo, dado su elemento.
    private NodoArbol obtenerNodo(NodoArbol nodo, Object elemento) {
        NodoArbol aux = null;
        if (aux == null && nodo.getElem() == elemento) {
            aux = nodo;
        } else if (aux == null && nodo.getIzquierdo() != null) {
            aux = obtenerNodo(nodo.getIzquierdo(), elemento);
        }
        if (aux == null && nodo.getDerecho() != null) {
            aux = obtenerNodo(nodo.getDerecho(), elemento);
        }

        return aux;
    }

    //Crea una lista en posOrden.
    public Lista listarPreorden() {
        Lista lista = new Lista();
        auxListarPreorden(this.raiz, lista, 1);
        return lista;
    }

    //Metodo recursivo auxiliar para listar el arbol en pre-orden.
    private void auxListarPreorden(NodoArbol nodo, Lista lista, int pos) {
        if (nodo != null) {
            lista.insertar(nodo.getElem(), pos);
            pos++;
            auxListarPreorden(nodo.getDerecho(), lista, pos);
            auxListarPreorden(nodo.getIzquierdo(), lista, pos);
        }
    }

    public Lista listarInorden() {
        Lista lista = new Lista();
        auxListarInorden(this.raiz, lista, 1);
        return lista;
    }

    //Metodo recursivo auxiliar para listar el arbol en in-orden.
    private void auxListarInorden(NodoArbol nodo, Lista lista, int pos) {
        if (nodo != null) {
            auxListarInorden(nodo.getDerecho(), lista, pos);
            lista.insertar(nodo.getElem(), pos);
            auxListarInorden(nodo.getIzquierdo(), lista, pos);
        }
    }

    public Lista listarPosorden(){
        Lista lista=new Lista();
        auxListarPosorden(this.raiz,lista,1);
        return lista;
    }
    
    //Metodo recursivo auxiliar para listar el arbol en pos-orden.
    private void auxListarPosorden (NodoArbol nodo, Lista lista, int pos){
        if (nodo != null){
            lista.insertar(nodo.getElem(),pos);
            auxListarPosorden(nodo.getDerecho(),lista,pos);
            auxListarPosorden(nodo.getIzquierdo(),lista,pos);
        }
    }
    
    public Lista listarNiveles(){
        Lista lista=new Lista();
        auxListarNiveles(lista,this.raiz);
        return lista;
    }
    
    private void auxListarNiveles(Lista lista,NodoArbol nodo){
        if (nodo != null){
            lista.insertar(nodo.getElem(), 1);
            auxListarNiveles(lista,nodo.getIzquierdo());
            auxListarNiveles(lista,nodo.getDerecho());
            
        }
    }
    
    public boolean esVacio() {
        return (this.raiz == null);
    }

    //Devuelve un numero con la altura del arbol.
    public int altura() {
        int i = -1, a, b;
        if (this.raiz != null) {
            a = alturaAux(this.raiz.getIzquierdo());
            b = alturaAux(this.raiz.getDerecho());
            if (a >= b) {
                i = a;
            } else {
                i = b;
            }
        }
        return i;
    }
    
    //Metodo auxiliar que calcula la altura de un arbol.
    private int alturaAux(NodoArbol nodo){
        int a=0,b=0;
        if (nodo != null){
            a = alturaAux(nodo.getIzquierdo()) + 1;
            
            b = alturaAux(nodo.getDerecho()) + 1;
        }
        if (a>=b)
            return a;
        else
            return b;
    }
    
    //Recibe un elemento y devuelve el nivel del nodo..
    public int nivel(Object elemento){
        int i;
        if (this.raiz.getElem() == elemento){
            i=0;
        } else {
            i = auxNivel(elemento,this.raiz.getIzquierdo(),1);
            if (i == -1){
                i = auxNivel(elemento,this.raiz.getDerecho(),1);
            }
        }
        return i;
    }
    
    //Metodo auxiliar que devuelve el nivel donde se encuentra un nodo, dado un objeto.
    private int auxNivel(Object elemento, NodoArbol nodo, int nivel) {
        int i = -1;
        if (nodo.getElem() != elemento) {
            if (nodo.getIzquierdo() != null) {
                i = auxNivel(elemento, nodo.getIzquierdo(), nivel + 1);
            }
            if (nodo.getDerecho() != null && i == -1) {
                i = auxNivel(elemento, nodo.getDerecho(), nivel + 1);
            }
        } else {
            i = nivel;
        }
        return i;
    }

    public void vaciar() {
        this.raiz = null;
    }

    //Recibe un elemento y devuelve el elemento del padre (primera ocurrencia).
    public Object padre(Object elemento) {
        Object objeto;
        NodoArbol aux = auxPadre(elemento, this.raiz);
        if (aux != null) {
            objeto = aux.getElem();
        } else {
            objeto = "El objeto no se encuentra en el arbol o es la raiz del mismo";
        }
        return objeto;
    }

    //Metodo auxiliar que devuelve el nodo padre dado un elemento hijo.
    private NodoArbol auxPadre(Object elemento, NodoArbol nodo) {
        NodoArbol aux = null;
        if (nodo != null && aux == null) {
            if (nodo.getIzquierdo() != null) {
                if (nodo.getIzquierdo().getElem() == elemento) {
                    aux = nodo;
                } else {
                    aux = auxPadre(elemento, nodo.getIzquierdo());
                }
            }
            if (nodo.getDerecho() != null && aux == null) {
                if (nodo.getDerecho().getElem() == elemento) {
                    aux = nodo;
                } else {
                    aux = auxPadre(elemento, nodo.getDerecho());
                }
            }

        }
        return aux;
    }
    
    
    public String toString(){
        String cadena;
        cadena=auxtoString(this.raiz);
        return cadena;
    }
    
    //Metodo auxiliar que crea un String con cada nodo del arbol.
    private String auxtoString(NodoArbol nodo){
        String cadena="";
        if (nodo != null){
            cadena += "Raiz: "+nodo.getElem();
            if (nodo.getIzquierdo() != null){
                cadena += " HI: "+nodo.getIzquierdo().getElem();
            } else {
                cadena +=" HI: null ";
            }
            if (nodo.getDerecho() != null){
                cadena += " HD: "+nodo.getDerecho().getElem()+"\n";
            } else {
                cadena += " HD: null \n";
            }
            cadena += auxtoString(nodo.getIzquierdo());
            cadena += auxtoString(nodo.getDerecho());
        }
        return cadena;
    }
}